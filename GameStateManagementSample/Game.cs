#region File Description
//-----------------------------------------------------------------------------
// Game.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using GameStateManagement;
using GameStateManagementSample.Screens;
//using Microsoft.Phone.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
//using Microsoft.Advertising.Mobile.Xna;
namespace GameStateManagementSample
{
    /// <summary>
    /// Sample showing how to manage different game states, with transitions
    /// between menu screens, a loading screen, the game itself, and a pause
    /// menu. This main game class is extremely simple: all the interesting
    /// stuff happens in the ScreenManager component.
    /// </summary>
    public class GameStateManagementGame : Microsoft.Xna.Framework.Game
    {
        BackgroundMusicManager musicManager;
        Song music;
        GraphicsDeviceManager graphics;
        ScreenManager screenManager;
        ScreenFactory screenFactory;
        /// <summary>
        /// The main game constructor.
        /// </summary>
        public GameStateManagementGame()
        {

            graphics.PreferredBackBufferHeight = 1280;
            graphics.PreferredBackBufferWidth = 720;
            Content.RootDirectory = "Content";

            musicManager = new BackgroundMusicManager(this);
            musicManager.PromptGameHasControl += MusicManagerPromptGameHasControl;
            musicManager.PlaybackFailed += MusicManagerPlaybackFailed;
            Components.Add(musicManager);
            this.Services.AddService(typeof(BackgroundMusicManager), musicManager);

            graphics = new GraphicsDeviceManager(this);
            TargetElapsedTime = TimeSpan.FromTicks(333333);
            Accelerometer.Initialize();

#if WINDOWS_PHONE
            graphics.IsFullScreen = true;

            // Choose whether you want a landscape or portait game by using one of the two helper functions.
            InitializeLandscapeGraphics();
            // InitializePortraitGraphics();
#endif

            // Create the screen factory and add it to the Services
            screenFactory = new ScreenFactory();
            Services.AddService(typeof(IScreenFactory), screenFactory);

            // Create the screen manager component.
            screenManager = new ScreenManager(this);
            Components.Add(screenManager);
#if DEBUG
            Components.Add(new FrameRateMonitor(this, true));
#endif
#if WINDOWS_PHONE
            // Hook events on the PhoneApplicationService so we're notified of the application's life cycle
            Microsoft.Phone.Shell.PhoneApplicationService.Current.Launching += 
                new EventHandler<Microsoft.Phone.Shell.LaunchingEventArgs>(GameLaunching);
            Microsoft.Phone.Shell.PhoneApplicationService.Current.Activated += 
                new EventHandler<Microsoft.Phone.Shell.ActivatedEventArgs>(GameActivated);
            Microsoft.Phone.Shell.PhoneApplicationService.Current.Deactivated += 
                new EventHandler<Microsoft.Phone.Shell.DeactivatedEventArgs>(GameDeactivated);
#else
            // On Windows and Xbox we just add the initial screens
            AddInitialScreens();
#endif
        }

        protected override void LoadContent()
        {
            // Load the song
            music = Content.Load<Song>("ouro");
            // Instruct our manager to play our background music.
            if (ToggleSoundScreen.musicOn == true)
            {
                musicManager.Play(music);
            }
        }

        private void AddInitialScreens()
        {
            // Activate the first screens.
            screenManager.AddScreen(new BackgroundScreen(), null);

            // We have different menus for Windows Phone to take advantage of the touch interface
#if WINDOWS_PHONE
            screenManager.AddScreen(new MainMenuScreen(), null);
#else
            screenManager.AddScreen(new MainMenuScreen(), null);
#endif
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        protected override void Draw(GameTime gameTime)
        {
            graphics.GraphicsDevice.Clear(Color.Black);

            // The real drawing happens inside the screen manager component.
            base.Draw(gameTime);
        }

#if WINDOWS_PHONE
        /// <summary>
        /// Helper method to the initialize the game to be a portrait game.
        /// </summary>
        private void InitializePortraitGraphics()
        {
            graphics.PreferredBackBufferWidth = 480;
            graphics.PreferredBackBufferHeight = 800;
        }

        /// <summary>
        /// Helper method to initialize the game to be a landscape game.
        /// </summary>
        private void InitializeLandscapeGraphics()
        {
            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 480;
        }

        void GameLaunching(object sender, Microsoft.Phone.Shell.LaunchingEventArgs e)
        {
            AddInitialScreens();
        }

        void GameActivated(object sender, Microsoft.Phone.Shell.ActivatedEventArgs e)
        {
            // Try to deserialize the screen manager
            if (!screenManager.Activate(e.IsApplicationInstancePreserved))
            {
                // If the screen manager fails to deserialize, add the initial screens
                AddInitialScreens();
            }
        }

        void GameDeactivated(object sender, Microsoft.Phone.Shell.DeactivatedEventArgs e)
        {
            // Serialize the screen manager when the game deactivated
            screenManager.Deactivate();
        }
#endif
        #region Music Manager Event Handlers

        private void MusicManagerPromptGameHasControl(object sender, EventArgs e)
        {
            // Show a message box to see if the user wants to turn off their music for the game's music.
            Guide.BeginShowMessageBox(
                "Use game music?",
                "Would you like to turn off your music to listen to the game's music?",
                new[] { "Yes", "No" },
                0,
                MessageBoxIcon.None,
                result =>
                {
                    // Get the choice from the result
                    int? choice = Guide.EndShowMessageBox(result);

                    // If the user hit the yes button, stop the media player. Our music manager will
                    // see that we have a song the game wants to play and that the game will now have control
                    // and will automatically start playing our game song.
                    if (choice.HasValue && choice.Value == 0)
                        MediaPlayer.Stop();
                },
                null);
        }

        private void MusicManagerPlaybackFailed(object sender, EventArgs e)
        {
            // We're going to show a message box so the user knows why music didn't start playing.
            Guide.BeginShowMessageBox(
                "Music playback failed",
                "Music playback cannot begin if the phone is connected to a PC running Zune.",
                new[] { "Ok" },
                0,
                MessageBoxIcon.None,
                null,
                null);
        }

        #endregion
    }
}
