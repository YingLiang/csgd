using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GameStateManagementSample.Screens
{
    class Humour
    {
        //add text into this array and call the index required
        public static string[] speeches = { "do not squish me", "stop squishing me", "have you always been defiant like this?", "you are going to make your parents sad you know",
        "ok fine. do what you want.", "because after you had your fill will i be free from your clutches.", "had enough?", "ok stop.", "stop it.", "please?", "pretty please with a cherry on top?",
        "ok tough guy, have it your way", "what if i told you that,", "i am the king of all slimes,", "and the more you submit me to your torture,",
        "the worse your suffering would be when my brethren arrives.", "im sorry for lying. please let me go", "ok", "what if i told you that,",
        "by squishing me more,", "you are releasing deadly chemicals into the air,","that have the capability to,","completely infect and destroy,",
        "the whole of mankind?","yep, thats right.","everybody died.","including you.","your friends.","your loved ones.","your family.","gone.",
        "poof, just like smoke.","including you.","because of you.","just because you wanted to squish a slime","so how does it feel,","knowing that you have doomed,","the whole of humanity,","due to your selfish wish of",
        "squishing a slime?","guilt?","regret?","remorse?","does your conscience prick at your brain?","oh wait. none of these matter.",
        "because you are dead.","me?","i'm not.","i'm just a sprite.","consisting of nothing more than pixels.","you on the other hand,",
        "a flesh and blood human being, ","turned into a lump of","unrecognizable meat,","due to the virus rapidly","expanding and invading your body",
        "*yawn*","i feel really tired","this squishing is getting too old...","...Zzz...","..zZz..","..Mmph..","Senpai...","WeiFeng senpai...",
        "...notice me...senpai...","the feelings...i have...for you...","...will never...fade away...","...it will never...falter...","...that's why...",
        "...","*awake*","well that was a nice nap.","and i see that you are still here.","allow us to,","bury the hatchet.","and put the past behind us.",
        "let us start anew,","in the hope that we become friends.","how about i tell you something about myself?","yes, that would make for an interesting conversation",
        "in a land","far far away,","there lived a peasant slime,","whose name was too insignificant to remember","he sold straw hats for a living.",
        "though life was tough for him,","he was fortunate enough to meet 2 other slimes.","there was an instant spark of attraction between them",
        "in those times, however,","their land was in chaos","due to the rebellion of the red bandannas.","in the middle of an oak forest,",
        "the three slimes underwent an oath","to protect their troubled land","from the red bandannas","and swore to defend their emperor.",
        "having received fundings from gold merchants,","this unknown slime and his followers","organized an army and mobilized to the front lines",
        "...","...","...","...","ah i'm sorry.","must have.","..Zz..","...zzZ...","...","*blink*","nope.","i was not asleep.","anyway, where was i?",
        "ah yes.","after achieving miracles on the frontlines,","the unknown slime was promoted to a baron over a plot of land.","in his mighty castle",
        "where he would swagger about,","he found a very interesting piece of artifact.","upon coming into contact with it,","he was sucked into its endless void",
        "that was seemingly impossible given its cardboard-thin size.","his servants eventually found the artifact,","and imprinted upon its glowing surface",
        "were the words:"};
    }
}
