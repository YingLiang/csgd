#region File Description
//-----------------------------------------------------------------------------
// GameplayScreen.cs
//
// Microsoft XNA Community Game Platform
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#endregion

#region Using Statements
using System;
using System.Threading;
//using System.Windows.Threading;
using System.IO.IsolatedStorage;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using GameStateManagement;
using Microsoft.Xna.Framework.Audio;
//using Newtonsoft.Json;
#endregion

namespace GameStateManagementSample.Screens
{
    /// <summary>
    /// This screen implements the actual game logic. It is just a
    /// placeholder to get the idea across: you'll probably want to
    /// put some more interesting gameplay in here!
    /// </summary>
    class GameplayScreen : GameScreen
    {
        #region Fields

        ContentManager content;
        SpriteFont gameFont;
        SoundEffect slimeSound;
        Texture2D background;
        int transparent = 100;
        //texture for the text
        Texture2D text;
        Vector2 textPosition;
        //texture for slime in level 1
        Texture2D slime;
        //texture for slime in level 2
        Texture2D slime2;
        //texture for slime in level 3
        Texture2D slime3;
        //texture for slime in level 4
        Texture2D slime4;
        //texture for slime in level 5
        Texture2D slime5;
        Texture2D slimex;
        Texture2D slime2x;
        Texture2D slime3x;
        Texture2D slime4x;
        Texture2D slime5x;
        Texture2D tslimex;
        Texture2D tslime2x;
        Texture2D tslime3x;
        Texture2D tslime4x;
        Texture2D tslime5x;
        //position of slime texture
        Vector2 slimePosition = new Vector2(0, 0);
        //position of level text
        Vector2 levelPosition;// = new Vector2(10, 10);
        //position of time text
        Vector2 timingPosition;// = new Vector2(620, 450);
        //position of score text
        Vector2 scorePosition;// = new Vector2(620, 410);
        Vector2 bgPosition = new Vector2(0, 0);
        static Random random = new Random();
        //DispatcherTimer timer = new DispatcherTimer();
        int time;
        string timerTxt = "";
        int level = 1; // keep track of level
        int pointsGained;// Changing the amount of point gains according to level
        IsolatedStorageFile myStorage;
        string filename = "ranking";
        float pauseAlpha;
        double timer;
        InputAction pauseAction;
        GamePadState currentState;
        GamePadState lastState;
        static DateTime startRumble;
        TimeSpan timeSpan;
        bool currentvibrate = false;
        bool lastvibrate = false;
        int score1;
        #endregion

        #region Initialization


        /// <summary>
        /// Constructor.
        /// </summary>
        public GameplayScreen()
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
            pauseAction = new InputAction(
                new Buttons[] { Buttons.Start },
                new Keys[] { Keys.Escape },
                true);

            //timer.Interval = new TimeSpan(0, 0, 0, 1);
            //timer.Tick += new EventHandler(timer_Tick);
            //timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            time++;
        }

        /// <summary>
        /// Load graphics content for the game.
        /// </summary>
        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                gameFont = content.Load<SpriteFont>("gamefont");
                slimeSound = content.Load<SoundEffect>("Squish");

                this.text = this.content.Load<Texture2D>("normaltext");
                this.slime = this.content.Load<Texture2D>("slimelevel1");
                this.slime2 = this.content.Load<Texture2D>("slimelevel2");
                this.slime3 = this.content.Load<Texture2D>("slimelevel3");
                this.slime4 = this.content.Load<Texture2D>("slimelevel4");
                this.slime5 = this.content.Load<Texture2D>("slimelevel5");
                this.slimex = this.content.Load<Texture2D>("slimelevel1untouched");
                this.slime2x = this.content.Load<Texture2D>("slimelevel2untouched");
                this.slime3x = this.content.Load<Texture2D>("slimelevel3untouched");
                this.slime4x = this.content.Load<Texture2D>("slimelevel4untouched");
                this.slime5x = this.content.Load<Texture2D>("slimelevel5untouched");
                this.tslimex = this.content.Load<Texture2D>("tsundereslime1");
                this.tslime2x = this.content.Load<Texture2D>("tsundereslime2");
                this.tslime3x = this.content.Load<Texture2D>("tsundereslime3");
                this.tslime4x = this.content.Load<Texture2D>("tsundereslime4");
                this.tslime5x = this.content.Load<Texture2D>("tsundereslime5");
                this.background = this.content.Load<Texture2D>("gameplaybackground");
                EnabledGestures = GestureType.Tap | GestureType.Flick;
                lastState = GamePad.GetState(PlayerIndex.One);
                // A real game would probably have more content than this sample, so
                // it would take longer to load. We simulate that by delaying for a
                // while, giving you a chance to admire the beautiful loading screen.
                Thread.Sleep(1000);

                // once the load has finished, we use ResetElapsedTime to tell the game's
                // timing mechanism that we have just finished a very long frame, and that
                // it should not try to catch up.
                ScreenManager.Game.ResetElapsedTime();
            }

        }

        public override void Deactivate()
        {
            /*if (ScreenManager.Vibrator != null)
            {
                ScreenManager.Vibrator.Stop();
            }*/

            base.Deactivate();
        }

        /// <summary>
        /// Unload graphics content used by the game.
        /// </summary>
        public override void Unload()
        {
            content.Unload();
        }

        #endregion

        #region Increments score
        private void updateScore()
        {
            switch (level)
            {
                case 1: pointsGained = 2;
                    break;
                case 2: pointsGained = 10;
                    break;
                case 3: pointsGained = 50;
                    break;
                case 4: pointsGained = 250;
                    break;
                case 5: pointsGained = 1250;
                    break;
            }
            Player.score += pointsGained;
        }
        #endregion

        #region Updates Level
        private void updateLevel()
        {
            if (Player.score >= 100 && Player.score < 1000)
            {
                level = 2;

            }
            else if (Player.score >= 1000 && Player.score < 7500)
            {
                level = 3;
            }
            else if (Player.score >= 7500 && Player.score < 50000)
            {
                level = 4;
            }
            else if (Player.score >= 50000 & Player.score < 112500)
            {
                level = 5;
            }
            else if (Player.score >= 112500)
            {
                //insert win message or something here
                //timer.Stop();
                Player.timing = time;
                //saveData();
                LoadingScreen.Load(ScreenManager, true, PlayerIndex.One, new EndScreen());
            }

        }
        #endregion

        #region SoundEffect
        private void slimeNoise()
        {
            if (ToggleSoundScreen.sfxOn == true)
            {
                slimeSound.Play();
            }
        }
        #endregion

        #region Check Level
        private void checkLevel()
        {

            if (Player.score == 100 || Player.score == 1000 || Player.score == 7500 || Player.score == 50000)
            {
                currentvibrate = true;
                //if (currentvibrate == true)
                //score1 -= 1;

                //score1 += 1;
              
                //GamePad.SetVibration(PlayerIndex.One, 20.0f, 20.0f);
                //startRumble = DateTime.Now;
                //timer = 0.0;
                //if (timer > 0.5)
                //        GamePad.SetVibration(PlayerIndex.One, 0.0f, 0.0f);
                //

            }
            /*switch (Player.score)
            {
                case 100:
                    timer = 0;
                    GamePad.SetVibration(PlayerIndex.One, 20.0f, 20.0f);
                    if (timer > 3.0)
                        GamePad.SetVibration(PlayerIndex.One, 0.0f, 0.0f);
                    break;
                case 1000:
                    timer = 0;
                    GamePad.SetVibration(PlayerIndex.One, 20.0f, 20.0f);
                    if (timer > 3.0)
                        GamePad.SetVibration(PlayerIndex.One, 0.0f, 0.0f);
                    break;
                case 7500:
                    timer = 0;
                    GamePad.SetVibration(PlayerIndex.One, 20.0f, 20.0f);
                    if (timer > 3.0)
                        GamePad.SetVibration(PlayerIndex.One, 0.0f, 0.0f);
                    break;
                case 50000:
                    timer = 0;
                    GamePad.SetVibration(PlayerIndex.One, 20.0f, 20.0f);
                    if (timer > 3.0)
                        GamePad.SetVibration(PlayerIndex.One, 0.0f, 0.0f);
                    break;
            }//to vibrate when next level achieved*/
        }
        #endregion

        #region Save Data
        /*void saveData() 
        {
            Leaderboard ranking = new Leaderboard();
            ranking.checkPlayerScore(Player.timing);
            myStorage = IsolatedStorageFile.GetUserStoreForApplication();
            //string json = JsonConvert.SerializeObject(ranking);

            try
            {
                IsolatedStorageFileStream stream = myStorage.OpenFile(filename, System.IO.FileMode.Create);
                byte[] input = System.Text.Encoding.UTF8.GetBytes(json);
                stream.Write(input, 0, input.Length);
                stream.Close();
                System.Diagnostics.Debug.WriteLine("saving worked");
            }
            catch (Exception ex) 
            {
                System.Diagnostics.Debug.WriteLine("Error saving data: " + ex.Message);
            }
        }*/
        #endregion


        #region Update and Draw


        /// <summary>
        /// Updates the state of the game. This method checks the GameScreen.IsActive
        /// property, so the game will stop updating when the pause menu is active,
        /// or if you tab away to a different application.
        /// </summary>
        public override void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                       bool coveredByOtherScreen)
        {
            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            textPosition = new Vector2(viewport.TitleSafeArea.Left + 100, viewport.TitleSafeArea.Bottom + 20);
            base.Update(gameTime, otherScreenHasFocus, false);

            // Gradually fade in or out depending on whether we are covered by the pause screen.
            if (coveredByOtherScreen)
                pauseAlpha = Math.Min(pauseAlpha + 1f / 32, 1);
            else
                pauseAlpha = Math.Max(pauseAlpha - 1f / 32, 0);

            if (IsActive)
            {
                timerTxt = time + "s";
                timer += gameTime.ElapsedGameTime.TotalSeconds;
                Vector3 acceleration = Accelerometer.GetState().Acceleration;
                int track;//track the amount of X,Y,Z from acceleration;
                track = (int)acceleration.X + (int)acceleration.Y + (int)acceleration.Z;
                if (track >= 0.1)
                {
                    updateScore();
                    updateLevel();
                    checkLevel();
                    slimeNoise();
                }
                currentState = GamePad.GetState(PlayerIndex.One);
                if (currentState.Buttons.A == ButtonState.Pressed && lastState.Buttons.A == ButtonState.Released)
                {
                    updateScore();
                    updateLevel();
                    checkLevel();
                    slimeNoise();
                }
                lastState = currentState;
                if (currentvibrate == true)
                {

                    startRumble = DateTime.Now;
                    GamePad.SetVibration(PlayerIndex.One, 1.0f, 1.0f);
                    currentvibrate = false;
                    lastvibrate = true;
                }
                if(lastvibrate == true){
                    timeSpan = DateTime.Now - startRumble;
                    if (timeSpan.TotalSeconds >= 1.0)
                    {
                        GamePad.SetVibration(PlayerIndex.One, 0, 0);
                        Player.score += 990;
                        lastvibrate = false;
                    }

                }
                if (transparent < 255)
                {
                    transparent++;
                }
               
            }//if game is active

        }


        /// <summary>
        /// Lets the game respond to player input. Unlike the Update method,
        /// this will only be called when the gameplay screen is active.
        /// </summary>
        public override void HandleInput(GameTime gameTime, InputState input)
        {
            if (input == null)
                throw new ArgumentNullException("input");

            // Look up inputs for the active player profile.
            int playerIndex = (int)ControllingPlayer.Value;
            PlayerIndex player;
            if (pauseAction.Evaluate(input, ControllingPlayer, out player))
            {
                ScreenManager.AddScreen(new PauseMenuScreen(), ControllingPlayer);
            }
            else
            {
                foreach (GestureSample gs in input.Gestures)
                {
                    switch (gs.GestureType)
                    {
                        case GestureType.Tap:
                            updateScore();
                            updateLevel();
                            checkLevel();
                            slimeNoise();
                            break;
                        case GestureType.Flick:
                            updateScore();
                            updateLevel();
                            checkLevel();
                            slimeNoise();
                            break;
                    }//check through each type of gesture
                }//goes through gestures in input.Gestures
            }
        }


        /// <summary>
        /// Draws the gameplay screen.
        /// </summary>
        public override void Draw(GameTime gameTime)
        {
            ScreenManager.GraphicsDevice.Clear(ClearOptions.Target,
                                               Color.CornflowerBlue, 0, 0);
            Viewport viewport = ScreenManager.GraphicsDevice.Viewport;
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            Rectangle fullscreen = new Rectangle(0, 0, viewport.Width, viewport.Height);
            scorePosition = new Vector2(viewport.TitleSafeArea.Right - 120, viewport.TitleSafeArea.Bottom - 30);
            levelPosition = new Vector2(viewport.TitleSafeArea.Left, viewport.TitleSafeArea.Top);
            timingPosition = new Vector2(viewport.TitleSafeArea.Right - 90, viewport.TitleSafeArea.Bottom - 30);
            spriteBatch.Begin();
            spriteBatch.Draw(background, fullscreen, Color.White);
            spriteBatch.DrawString(gameFont, "Points: " + Player.score, scorePosition, Color.Red);
            spriteBatch.DrawString(gameFont, "Level: " + level, levelPosition, Color.Red);
            spriteBatch.DrawString(gameFont, "Time: " + timerTxt, timingPosition, Color.Red);
            if (level == 1)
            {
                spriteBatch.Draw(tslimex, slimePosition, Color.White);
            }
            else if (level == 2)
            {
                spriteBatch.Draw(tslime2x, slimePosition, Color.White);
            }
            else if (level == 3)
            {
                spriteBatch.Draw(tslime3x, slimePosition, Color.White);
            }
            else if (level == 4)
            {
                spriteBatch.Draw(tslime4x, slimePosition, Color.White);
            }
            else if (level == 5)
            {
                spriteBatch.Draw(tslime5x, slimePosition, Color.White);
            }//Draws texture according to the level

            spriteBatch.Draw(text, textPosition, Color.FromNonPremultiplied(255, 255, 255, transparent));
            spriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);

                ScreenManager.FadeBackBufferToBlack(alpha);
            }//to solve flickering issues
        }


        #endregion
    }
}
