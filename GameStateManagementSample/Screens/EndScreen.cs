using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO.IsolatedStorage;
//using Newtonsoft.Json;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using GameStateManagement;


namespace GameStateManagementSample.Screens
{
    class EndScreen : GameScreen
    {
        ContentManager content;
        SpriteFont gameFont;

        string message = "You defeated the slime and won!";
        Vector2 messagePosition = new Vector2(200, 100);

        string leaderboardTxt = "Leaderboards";
        Vector2 leaderboardPosition = new Vector2(500, 180);
        
        string firstPlayerTxt = "1. ";
        int firstPlayer = 0;
        Vector2 firstPlayerPosition = new Vector2(550, 230);

        string secondPlayerTxt = "2. ";
        int secondPlayer = 0;
        Vector2 secondPlayerPosition = new Vector2(550, 280);

        string thirdPlayerTxt = "3. ";
        int thirdPlayer = 0;
        Vector2 thirdPlayerPosition = new Vector2(550, 330);

        IsolatedStorageFile myStorage;
        string filename = "ranking";

        Button retryButton;
        Button exitButton;

        public EndScreen() 
        {
            TransitionOnTime = TimeSpan.FromSeconds(1.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);

            loadData();

            retryButton = new Button("Retry");
            retryButton.Alpha = 1;
            retryButton.Position.X = 100;
            retryButton.Position.Y = 200;
            retryButton.Tapped += new EventHandler<EventArgs>(retryButton_Tapped);

            exitButton = new Button("Exit");
            exitButton.Alpha = 1;
            exitButton.Position.X = 100;
            exitButton.Position.Y = 300;
            exitButton.Tapped += new EventHandler<EventArgs>(exitButton_Tapped);
        }

        void retryButton_Tapped(object sender, EventArgs e)
        {
            Player.score = 0;
            Player.timing = 0;
            LoadingScreen.Load(ScreenManager, true, PlayerIndex.One, new GameplayScreen());
        }

        void exitButton_Tapped(object sender, EventArgs e)
        {
            Player.score = 0;
            Player.timing = 0;
            LoadingScreen.Load(ScreenManager, true, null, new BackgroundScreen(),
                                                           new MainMenuScreen());
        }

        #region Load Data
        void loadData() 
        {
            myStorage = IsolatedStorageFile.GetUserStoreForApplication();

            if (myStorage.FileExists(filename)) 
            {
                try
                {
                    IsolatedStorageFileStream stream = myStorage.OpenFile(filename, System.IO.FileMode.Open);
                    byte[] output = new byte[stream.Length];
                    int result = stream.Read(output, 0, output.Length);

                    string json = System.Text.Encoding.UTF8.GetString(output, 0, output.Length);

                    //Leaderboard ranking = JsonConvert.DeserializeObject<Leaderboard>(json);
                    //firstPlayer = ranking.firstPlayerTiming;
                    //secondPlayer = ranking.secondPlayerTiming;
                    //thirdPlayer = ranking.thirdPlayerTiming;

                    stream.Close();
                    System.Diagnostics.Debug.WriteLine("loading worked");
                }
                catch (Exception ex) 
                {
                    System.Diagnostics.Debug.WriteLine("Error loading data: " + ex.Message);
                }
            }
        }
        #endregion

        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                gameFont = content.Load<SpriteFont>("gamefont");
                
                EnabledGestures = GestureType.Tap;
                // A real game would probably have more content than this sample, so
                // it would take longer to load. We simulate that by delaying for a
                // while, giving you a chance to admire the beautiful loading screen.
                Thread.Sleep(1000);

                // once the load has finished, we use ResetElapsedTime to tell the game's
                // timing mechanism that we have just finished a very long frame, and that
                // it should not try to catch up.
                ScreenManager.Game.ResetElapsedTime();
            }
            base.Activate(instancePreserved);
        }

        public override void Deactivate()
        {
            base.Deactivate();
        }

        public override void Unload()
        {
            content.Unload();
        }

        public override void HandleInput(GameTime gameTime, InputState input)
        {
            foreach (GestureSample gesture in input.Gestures)
            {
                if (gesture.GestureType == GestureType.Tap)
                {
                    retryButton.HandleTap(gesture.Position);
                    exitButton.HandleTap(gesture.Position);
                }
            }
            base.HandleInput(gameTime, input);
        }

        public override void Draw(GameTime gameTime)
        {
            //change background
            this.ScreenManager.GraphicsDevice.Clear(Color.LightYellow);
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;

            spriteBatch.Begin();
            spriteBatch.DrawString(gameFont, message, messagePosition, Color.Black);
            spriteBatch.DrawString(gameFont, leaderboardTxt, leaderboardPosition, Color.Black);
            spriteBatch.DrawString(gameFont, firstPlayerTxt + firstPlayer, firstPlayerPosition, Color.Black);
            spriteBatch.DrawString(gameFont, secondPlayerTxt + secondPlayer, secondPlayerPosition, Color.Black);
            spriteBatch.DrawString(gameFont, thirdPlayerTxt + thirdPlayer, thirdPlayerPosition, Color.Black);

            retryButton.Draw(this);
            exitButton.Draw(this);
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
