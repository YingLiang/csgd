using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using GameStateManagement;


namespace GameStateManagementSample.Screens
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class controls : GameScreen
    {
        ContentManager content;
        InputAction exitAction;
        Texture2D controller;
        SpriteBatch spriteBatch;

        public controls(Game game)
        {
            // TODO: Construct any child components here
        }
        public controls()
        {
            exitAction = new InputAction(new Buttons[] { Buttons.B}, new Keys[] { Keys.Escape }, true);
        }
        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>

        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                controller = content.Load<Texture2D>("controllerguide");
                Thread.Sleep(1000);
                ScreenManager.Game.ResetElapsedTime();
            }
        }
        public override void HandleInput(GameTime gameTime, InputState input)
        {
            /*int playerIndex = (int)ControllingPlayer.Value;
             PlayerIndex player;
             if (exitAction.Evaluate(input, ControllingPlayer, out player))
             {
                 this.ExitScreen();
                 ScreenManager.AddScreen(new BackgroundScreen(), ControllingPlayer);
                 ScreenManager.AddScreen(new MainMenuScreen(), ControllingPlayer);
             }
             base.HandleInput(gameTime, input);
         }*/
            if (GamePad.GetState(PlayerIndex.One).Buttons.B == ButtonState.Pressed)
            {
                this.ExitScreen();
                ScreenManager.AddScreen(new BackgroundScreen(), ControllingPlayer);
                ScreenManager.AddScreen(new MainMenuScreen(), ControllingPlayer);
            }
            /// <summary>
            /// Allows the game component to update itself.
            /// </summary>
            /// <param name="gameTime">Provides a snapshot of timing values.</param>
           
        }
        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin();
            spriteBatch.Draw(controller, Vector2.Zero, Color.White);
            spriteBatch.End();
        }
    }
}
