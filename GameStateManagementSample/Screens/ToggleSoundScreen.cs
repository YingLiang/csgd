using System;
using System.Collections.Generic;
using System.Threading;
using System.IO.IsolatedStorage;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using GameStateManagement;

namespace GameStateManagementSample.Screens
{
    class ToggleSoundScreen: GameScreen
    {
        ContentManager content;
        InputAction exitAction;

        //IsolatedStorageSettings saveGameStorage;
        
        Song music;
        BooleanButton musicButton;
        BooleanButton soundButton;

        public static bool sfxOn = true;
        public static bool musicOn = true;
        
        
        public ToggleSoundScreen()
        {
            this.EnabledGestures = GestureType.Tap;
            exitAction = new InputAction(new Buttons[] { Buttons.Back }, new Keys[] { Keys.Escape }, true);

            //loadButtonState();

            musicButton = new BooleanButton("Music", musicOn);
            musicButton.Alpha = 1;
            musicButton.Position.X = 280;
            musicButton.Position.Y = 170;
            musicButton.Tapped += new EventHandler<EventArgs>(musicButton_Tapped);

            soundButton = new BooleanButton("SFX", sfxOn);
            soundButton.Alpha = 1;
            soundButton.Position.X = 280;
            soundButton.Position.Y = 300;
            soundButton.Tapped += new EventHandler<EventArgs>(soundButton_Tapped);
        }

        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                // Load the song
                music = this.content.Load<Song>("ouro");
            }
        }

        public override void Deactivate()
        {

            base.Deactivate();
        }

        public override void HandleInput(GameTime gameTime, InputState input)
        {
            int playerIndex = (int)ControllingPlayer.Value;
            PlayerIndex player;
            if (exitAction.Evaluate(input, ControllingPlayer, out player))
            {
                this.ExitScreen();
                ScreenManager.AddScreen(new BackgroundScreen(), ControllingPlayer);
                ScreenManager.AddScreen(new MainMenuScreen(), ControllingPlayer);
            }
            else
            {
                foreach (GestureSample gesture in input.Gestures)
                {
                    if (gesture.GestureType == GestureType.Tap)
                    {
                        musicButton.HandleTap(gesture.Position);
                        soundButton.HandleTap(gesture.Position);
                    }
                }
            }
            base.HandleInput(gameTime, input);
        }

        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin();
            musicButton.Draw(this);
            soundButton.Draw(this);
            spriteBatch.End();
        }

        void musicButton_Tapped(object sender, EventArgs e)
        {
            BackgroundMusicManager mgr = (BackgroundMusicManager)ScreenManager.Game.Services.GetService(typeof(BackgroundMusicManager));
            if (mgr.IsGameMusicPlaying)
            {
                mgr.Stop();
                musicOn = false;
            }
            // Otherwise start playing the song.
            else
            {
                mgr.Play(music);
                musicOn = true;
            }
            //saveButtonState();
        }

        void soundButton_Tapped(object sender, EventArgs e) 
        {
            if (sfxOn == true)
            {
                sfxOn = false;
            }
            else 
            {
                sfxOn = true;
            }
            //saveButtonState();
        }

        /*void saveButtonState() 
        {
            if (saveGameStorage.Contains("musicButtonState"))
            {
                saveGameStorage["musicButtonState"] = musicOn;
            }
            else 
            {
                saveGameStorage.Add("musicButtonState", musicOn);
            }

            if (saveGameStorage.Contains("soundButtonState"))
            {
                saveGameStorage["soundButtonState"] = sfxOn;
            }
            else
            {
                saveGameStorage.Add("soundButtonState", sfxOn);
            }
            saveGameStorage.Save();
        }//Save the current button state
        */
        /*void loadButtonState() 
        {
            saveGameStorage = IsolatedStorageSettings.ApplicationSettings;
            if (saveGameStorage.Contains("musicButtonState"))
            {
                musicOn = (Boolean)saveGameStorage["musicButtonState"];
            }
            else 
            {
                musicOn = true;
            }

            if (saveGameStorage.Contains("soundButtonState"))
            {
                sfxOn = (Boolean)saveGameStorage["soundButtonState"];
            }
            else 
            {
                sfxOn = true;
            }
        }//Load the correct button state*/
    }
}
