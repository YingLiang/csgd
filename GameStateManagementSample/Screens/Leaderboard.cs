using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using GameStateManagement;

namespace GameStateManagementSample.Screens
{
    public class Leaderboard
    {
        public int firstPlayerTiming = 100;
        public int secondPlayerTiming = 200;
        public int thirdPlayerTiming = 300;

        //method to check incoming score
        public void checkPlayerScore(int timing) 
        {
            if (timing < firstPlayerTiming) 
            {
                thirdPlayerTiming = secondPlayerTiming;
                secondPlayerTiming = firstPlayerTiming;
                firstPlayerTiming = timing;
            }
            else if (timing < secondPlayerTiming) 
            {
                thirdPlayerTiming = secondPlayerTiming;
                secondPlayerTiming = timing;
            }
            else if (timing < thirdPlayerTiming) 
            {
                thirdPlayerTiming = timing;
            }
        }
    }
}
