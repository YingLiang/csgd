using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using GameStateManagement;


namespace GameStateManagementSample.Screens
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class GuideScreen : GameScreen
    {
        ContentManager content;
        InputAction exitAction;
        SpriteFont gameFont;
        SpriteFont headFont;
        Vector2 titlePosition = new Vector2(300, 40);
        Vector2 textPosition = new Vector2(5, 170);
        Vector2 textPosition2 = new Vector2(5, 200);
        Vector2 textPosition3 = new Vector2(5, 230);
        string text = "Tap, swipe/flick or shake the phone to earn points.";
        string text2 ="By reaching milestones, your slime will be automatically evolved!";
        string text3 ="Try and get as much points you can get!!";
        
        public GuideScreen()
        {
            exitAction = new InputAction(new Buttons[] { Buttons.B }, new Keys[] { Keys.Escape }, true);
        }
        
        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");
                gameFont = content.Load<SpriteFont>("gameFont");
                headFont = content.Load<SpriteFont>("headFont");
                Thread.Sleep(1000);
                ScreenManager.Game.ResetElapsedTime();
            }
        }
        
        public override void Deactivate()
        {

            base.Deactivate();
        }
        
        public override void HandleInput(GameTime gameTime, InputState input)
        {
            int playerIndex = (int)ControllingPlayer.Value;
            PlayerIndex player;
            if (exitAction.Evaluate(input, ControllingPlayer, out player))
            {
                this.ExitScreen();
                ScreenManager.AddScreen(new BackgroundScreen(), ControllingPlayer);
                ScreenManager.AddScreen(new MainMenuScreen(), ControllingPlayer);
            }
            base.HandleInput(gameTime, input);
        }
        
        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin();
            spriteBatch.DrawString(headFont,"How to play",titlePosition,Color.White);
            spriteBatch.DrawString(gameFont, text, textPosition, Color.White);
            spriteBatch.DrawString(gameFont, text2, textPosition2, Color.White);
            spriteBatch.DrawString(gameFont, text3, textPosition3, Color.White);
            spriteBatch.End();
        }
    }
}
