using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using GameStateManagement;

namespace GameStateManagementSample.Screens
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class CreditScreen : GameScreen
    {
        float pauseAlpha;

        SpriteFont gameFont;
        SpriteFont smallFont;

        Vector2 logoPosition = new Vector2 (400,40);
        Vector2 textPosition = new Vector2(340, 160);
        Vector2 sourcePosition = new Vector2(310, 240);
        Vector2 microPosition = new Vector2(310, 280);
        Vector2 soundReference = new Vector2(310, 320);
        Vector2 musicReference = new Vector2(310, 400);
        
        Texture2D logo;

        Vector2 schoolText = new Vector2 (330,60);
        Vector2 teamName = new Vector2(30,60);
        Vector2 member1 = new Vector2(30,120);
        Vector2 member2 = new Vector2(30,240);
        Vector2 member3 = new Vector2(30,360);

        ContentManager content;
        InputAction exitAction;
        
        public CreditScreen()
        {
            exitAction = new InputAction(new Buttons[] { Buttons.B }, new Keys[] { Keys.Escape }, true);
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Activate(bool instancePreserved)
        {
            if (!instancePreserved)
            {
                if (content == null)
                    content = new ContentManager(ScreenManager.Game.Services, "Content");

                smallFont = content.Load<SpriteFont>("referencefont");
                gameFont = content.Load<SpriteFont>("gamefont");
                logo = content.Load<Texture2D>("logo");

                Thread.Sleep(1000);
                ScreenManager.Game.ResetElapsedTime();
            }
        }

        public override void Deactivate()
        {

            base.Deactivate();
        }

        public override void HandleInput(GameTime gameTime, InputState input)
        {
            int playerIndex = (int)ControllingPlayer.Value;
            PlayerIndex player;
            if (exitAction.Evaluate(input, ControllingPlayer, out player))
            {
                this.ExitScreen();
                ScreenManager.AddScreen(new BackgroundScreen(), ControllingPlayer);
                ScreenManager.AddScreen(new MainMenuScreen(), ControllingPlayer);
            }
            base.HandleInput(gameTime, input);
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gametime) 
        {
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            spriteBatch.Begin();
            spriteBatch.DrawString(gameFont, "~MapleWarriors~", teamName,Color.White);
            spriteBatch.DrawString(gameFont, "Tan Ying Liang(1334128)\nDesigning", member1, Color.HotPink);
            spriteBatch.DrawString(gameFont, "Wilson Neo(1334313)\nProgramming", member2, Color.Gray);
            spriteBatch.DrawString(gameFont, "Gabriel Chia(1334706)\nProgramming", member3, Color.Green);
            spriteBatch.Draw(logo, logoPosition, Color.White);
            //spriteBatch.Draw(logo, logoPosition, Color.FromNonPremultiplied(255,255,255,100));
            spriteBatch.DrawString(gameFont, "School of Digital Media & Infocomm\nTechnology", textPosition, Color.White);
            spriteBatch.DrawString(gameFont, "Sources:", sourcePosition, Color.White);
            spriteBatch.DrawString(gameFont, "Microsoft", microPosition, Color.White);
            spriteBatch.DrawString(gameFont, "Sound 'Squish' by Mike Koenig\nLicensed under Attribution 3.0",soundReference, Color.White);
            spriteBatch.DrawString(smallFont, "'Ouroboros' Kevin MacLeod (incompetech.com)\nLicensed under Creative Commons: By Attribution 3.0\nhttp://creativecommons.org/licenses/by/3.0/", musicReference, Color.White);
            spriteBatch.End();

            // If the game is transitioning on or off, fade it out to black.
            if (TransitionPosition > 0 || pauseAlpha > 0)
            {
                float alpha = MathHelper.Lerp(1f - TransitionAlpha, 1f, pauseAlpha / 2);
                ScreenManager.FadeBackBufferToBlack(alpha);
            }//to solve flickering issues
        }
    }
}
